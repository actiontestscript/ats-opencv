#include "resolution.h"
namespace ats {
	Resolution::Resolution() {
		m_resolution["cga"] = Vec2i(320, 200);
		m_resolution["qvga"] = Vec2i(320, 240);
		m_resolution["vga"] = Vec2i(640, 480);
		m_resolution["ntsc"] = Vec2i(720, 480);
		m_resolution["svga"] = Vec2i(800, 600);
		m_resolution["xga"] = Vec2i(1024, 768);
		m_resolution["hd"] = Vec2i(1280, 720);
		m_resolution["wxga"] = Vec2i(1280, 800);
		m_resolution["sxga"] = Vec2i(1280, 1024);
		m_resolution["fullhd"] = Vec2i(1920, 1080);
		m_resolution["2k"] = Vec2i(2048, 1080);
		m_resolution["uhd-1"] = Vec2i(3840, 2160);
		m_resolution["4k"] = Vec2i(3840, 2160);
		m_resolution["8k"] = Vec2i(7680, 4320);
	}

	const Vec2i Resolution::getResolution(string resvalue) {
		if (m_resolution.find(resvalue) != m_resolution.end()) {
			return m_resolution[resvalue];
		}
		else {
			return Vec2i(0, 0);
		}
	}

}