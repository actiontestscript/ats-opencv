#pragma once
#ifndef _OPTIONS_H
#define _OPTIONS_H

#include "optionLigneCommande.h"
namespace ats {
	/*!
	*\brief Class OptionsData
	* Class OptionsData is a data class that contains all the options in program input parameters
	*/

	class OptionsData {
		std::map <string, OptionsStruct::strOptions> m_listOptions;			//!< map of all options

	public:
		/*!
		*\brief Constructor
		* Construcor of OptionsData
		*/
		OptionsData();

		/*!
		* \brief getOptions
		* Get options from map
		* \return map for all options
		*/
		const std::map <string, OptionsStruct::strOptions> getOptions() { return m_listOptions; }

	};
}
#endif // !_OPTIONS_H
