#pragma once
#ifndef _OPTION_LIGNE_COMMANDE_H
#define _OPTION_LIGNE_COMMANDE_H
#include "errors.h"
#include <map>
#include <regex>
using std::pair;
using std::map;

namespace ats{

	/*!
	* \brief Class OptionsStruct
	* Class set structur option
	*
	*/
	class OptionsStruct {
	public:
		struct strOptions {
			string key;						/*!< key option */
			string value;					/*!< value option */
			string detailsHelp;				/*!< details help option */
			string regValidate;				/*!< regular expression validate option */
			int errorNumber=0;				/*!< error number */
			bool visibility = true;			/*!< visibility option */
		};
	};

	/*!
	* \brief Class OptionLigneCommande
	* Class set and manage command line options
	*/
	class OptionLigneCommande {

	public:
		/*!
		* \brief Constructor
		* Constructor of class OptionLigneCommande
		* \param argc : number of arguments
		* \param argv : list of arguments
		*/
		OptionLigneCommande(int argc, char* argv[]);
	
		/*!
		* \brief Constructor
		* Constructor of class OptionLigneCommande
		* \param argc : number of arguments
		* \param argv : list of arguments
		* \param listOptions : list of options
		* \param errors : list of errors
		*/
		OptionLigneCommande(int argc, char* argv[], map <string, OptionsStruct::strOptions> listOptions, ats::Errors &errors);
	
		/*!
		*\brief SetOptions
		* Set options
		* \param key : key option
		* \param value : value option
		* \param detailHelp : details help option
		* \param regValidate : value regExp to validate parametre 
		*/
		const void setOptions(string key, string value, string detailHelp, string regValidate);
	
		/*!
		* \brief  setOptionsValue
		* Set value option
		* \param key : key option
		* \param value : value option
		*/
		const void setOptionsValue(string key, string value);
	
		/*!
		*\brief getOption
		* Get lists of options
		* \return list of options
		*/
		const map <string, OptionsStruct::strOptions> getOptions() { return m_listArgv; }
	
		/*!
		* \brief getOptionValue
		* Get value option
		* \param key : key option
		* \return string of value options
		*/
		const string getOptionValue(string key);

		/*!
		* \brief getOptionsError
		* Get error option
		* \return return a boolean at true if an error is detected when entering a parameter
		*/
		const bool getOptionsError() { return m_optionsError; };
	
		/*!
		* \brief readOptions
		* read the options enter in parameters
		*/
		const bool readOptions();
	
		/*!
		* \brief _checkInput
		* check input options
		* \return bool false if errors
		*/
		bool _checkInput() { return __checkInput(); };  //pour test
	
	private:
		int m_argc;												/*!< number of arguments */
		bool m_options;											/*!< options */
		map <string, OptionsStruct::strOptions> m_listArgv;		/*!< list of options */
		vector <string> m_argv;									/*!< list of arguments */
		bool m_optionsError;									/*!< options error */
		ats::Errors m_errors;										/*!< message errors */
		/*!
		* \brief _checkInput
		* check input options
		* \return bool false if errors
		*/
		bool __checkInput();									

	};
}
#endif

