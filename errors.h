#pragma once
#ifndef ERRORS_H
#define ERRORS_H
#include <iostream>
#include <string>
#include <vector>
#include "log.h"

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::to_string;


namespace ats {
	/*!
	* \class ErrorsData
	* \brief set data for error message
	*/
	class ErrorsData {
	public:
		struct ErrorInfo {
			int errorNumber = 0;		/*!< Error Number type int */
			string errorMessage;	/*!< Error Message type string */
			int gravity;			/*!< Error Gravity type int */
		};
	};
	/*!
	* \class Errors
	* \brief manage error in software
	*/

	class Errors {

	private:
		vector<ErrorsData::ErrorInfo> m_errors;	/*!< vector of ErrorsData */
		LogAts m_log;

	public:
		enum { INFO, WARNING, CRITICAL };	/*!< enum of gravity */
		/*!
		* \brief Constrcutor
		*
		* Constructor of Errors _ init m_errors
		*
		*/
		Errors();

		/*!
		* \brief getError
		*
		* get a structur of ErrorsData
		*
		* \param errorNumber : number of error
		* \return a struct of ErrorData
		*/
		const ErrorsData::ErrorInfo getError(int errorNumber);

		/*!
		* \brief display Error
		*
		* Display error message in console
		*
		* \param errorNumber : number of error
		*/

		void displayError(int errorNumber);
		void setAtsLog(LogAts &logAts){ m_log = logAts; }
	};


#endif // !ERRORS_H

}