# ats-opencv
Ats-OpenCv is a tool to count the number of shapes in a capture window via an acquisition device such as a webcam.

## Getting started
### for compiling :
download https://github.com/opencv/opencv/releases/download/4.8.0/opencv-4.8.0-windows.exe
Install opencv

### system environment variable : 
- OPENCV_HOME on c:\opencv\build

for example if installation directorie opencv : c:\opencv

### clone src project
clone git and exeute ats-opencv.sln 
(project visual studio 2022 norme iso 2022 C++ 20)

### for direct use :
copy directory Release in X64\build\Release.

## File description
### x64\build\Release :
* ats-opencv.exe 	: main application

### root
* ats-opencv.sln, ats-opencv.vcxproj, ats-opencv.filters, ats-opencv.user
    files project visual studio 2022
* ats-opencv.h , ats-opencv.cpp
    main file
* errors.h, errors.cpp
    Class ErrorsData and Errors. stores software errors information
* functions.h,functions.cpp 
    ats_openvc functions
* opencv.cpp    
    first file test
* optionLigneCommande.h, optionLigneCommande.cpp
    Class line command options
* options.h, options.cpp
    Class options in input parameters
* resolution.h, resolution.cpp
    Class resolution data

## Usage
### Default settings :
ats-opencv.exe <options>
#### options :
* -cap 0    		-> capture device webcam 0 value between 0 and 9
* -crop 0,0,0,0	    -> catch area x,y,width,height default value fullscreen pature device
* -fps 25			-> Frame per second value between 1 and 25 fps
* -rin fullhd		-> resolution display source fullhd (1920*1080) value : vga(640*480), svga (800*600), xga (1024*768), hd (1280*720), fullhd (1920*1080)
* -t 3 			-> capture time 10 seconds  value between 0 and 20 seconds
for t 0 an image capture is made and renamed to out.png in the user's temp directory

### Example in command line : 
ats-opencv -crop 100,100,500,300
	capture and count shapes in window pos x : 100, pos y : 100, whidth : 500, height : 300

### return information
* shapes-count= xxxx
* output-files=%temp%\out.mp4
information with newline \n after value

### save output files
The directory for outputfile is environment variable %TEMP%

## Status
in development

## Support
### Install ats-opencv (install compiled binaries files) : 
- in *'%APPDATA%/ats/tools/ats-opencv'* folder
- in *'%USERPROFILE%/ats-opencv'* folder
- in any folder defined using :
    - environment variable : ATS_OPENCV_HOME
    - system environment variable using Windows registry base :
	    *HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment\ATS_OPENCV_HOME*
## Roadmap

## Contributing

## Authors and acknowledgment


## License
* OpenCV  :	licence Apache 2 					: https://github.com/opencv/opencv/blob/master/LICENSE

	
