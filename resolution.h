#pragma once
#ifndef _RESOLUTION_H_
#define _RESOLUTION_H_
#include "optionLigneCommande.h"
#include <opencv2/opencv.hpp>
namespace ats {
	using cv::Vec2i;

	/*!
	* \brief Class Resolution
	* Class Resolution store data of screen resolution*
	*/

	class Resolution {
	private:
		map<string, Vec2i> m_resolution;				//!< Map of resolutions

	public:
		/*!
		*\brief Constructor
		* Constructor of class Resolution
		* initialize a list of resolution
		*/
		Resolution();

		/*!
		* \brief Get resolution
		* Get width and height of resolution
		* \param resvalue : string of resolution
		* \return cv::Vec2f vector 2 float width and height
		*/
		const Vec2i getResolution(string resvalue);

	};


}

#endif // !_RESOLUTION_H_

