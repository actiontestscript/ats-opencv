#include "optionLigneCommande.h"
namespace ats {
	OptionLigneCommande::OptionLigneCommande(int argc, char* argv[]) {
		m_argc = argc;
		m_options = (m_argc > 1);
		m_optionsError = false;
		std::copy(argv, argv + argc, std::back_inserter(m_argv));
	}

	OptionLigneCommande::OptionLigneCommande(int argc, char* argv[], map <string, OptionsStruct::strOptions> listOptions, ats::Errors &errors) :OptionLigneCommande(argc, argv) {
		m_errors = errors;
		m_listArgv = listOptions;
		m_optionsError = readOptions();
	}

	const void OptionLigneCommande::setOptions(string key, string value, string detailHelp, string regValidate) {
		OptionsStruct::strOptions aOptions{ key, value, detailHelp, regValidate };
		if (m_listArgv.find(key) != m_listArgv.end())
			m_listArgv[key] = aOptions;
		else
			m_listArgv.insert(std::pair<string, OptionsStruct::strOptions>(key, aOptions));
	}

	const void OptionLigneCommande::setOptionsValue(string key, string value) { if (m_listArgv.find(key) != m_listArgv.end()) m_listArgv[key].value = value; }

	const string OptionLigneCommande::getOptionValue(string key) { return (m_listArgv.find(key) != m_listArgv.end()) ? m_listArgv[key].value : ""; }

	const bool OptionLigneCommande::readOptions() {

		string options = "";
		bool argPar = false;
		if (m_argc == 2 && (m_argv[1] == "-h" || m_argv[1] == "/h" || m_argv[1] == "-help" || m_argv[1] == "/help" || m_argv[1] == "-?" || m_argv[1] == "/?")) {
			cout << "List of command line options:\n\n";
			for (auto a : m_listArgv) {
				if (a.second.visibility)
					cout << a.second.key << "\t" << a.second.detailsHelp << "\n\n";
			}
			return false;
		}
		else if (m_argc == 2 && (m_argv[1] == "-v" || m_argv[1] == "-version" )) {
			cout << "AtsOpenCV " << APP_VERSION ;
			return false;
		}

		else if (m_argc > 1) {
			for (int i = 1; i < m_argc; i++) {
				if (m_argv[i][0] == '-' && !argPar) {
					if (argPar) setOptionsValue(options, "");
					else argPar = true;
					options = m_argv[i];
					options = options.substr(1);
				}
				else {
					if (argPar) {
						setOptionsValue(options, m_argv[i]);
						options = "";
						argPar = false;
					}
				}
			}
			if (!__checkInput()) return false;
		}

		return true;
	}


	bool OptionLigneCommande::__checkInput() {
		for (map <string, OptionsStruct::strOptions>::iterator it = m_listArgv.begin(); it != m_listArgv.end(); ++it) {
			std::regex pattern(it->second.regValidate);
			if (!std::regex_match(it->second.value, pattern)) {
				//			cout << "l'option -" << it->first << " avec la valeur : " << it->second.value << " n'est pas conforme. \n" << it->second.detailsHelp << '\n';
				m_errors.displayError(it->second.errorNumber);
				return false;
			}
		}
		return true;
	}
}