#pragma once
#ifndef _LOG_H
#define _LOG_H
#include<String>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <filesystem>
#include <ctime>
#include "version.h"

namespace ats {

	class LogAts {
	private:
		std::string m_logFile{ "openCV.txt" };
		std::string m_logDir{ "ats_log" };
		std::string m_logPath{ "" };
		char m_sep{ '\\' };
		bool _getPath();
		bool _checkLogDir();
		bool _delLogDir(const bool &init);

	public:
		LogAts();
		LogAts(bool Init);
		char getSep() { return m_sep; }
		std::string getPathLogFile() { return m_logPath + m_logDir + getSep() + m_logFile; }
		void addLog(const std::string& logMessage);
	};

}//end namespace ats

#endif
