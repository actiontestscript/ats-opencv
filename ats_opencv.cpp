#include "ats_opencv.h"


//using namespace std;

int main(int argc, char* argv[]) {
	string dossierTemp = fs::temp_directory_path().string();
	LogAts logAts(true);
	logAts.addLog("start ats_opencv");
	logAts.addLog(getOpenCvArg(argc,argv)); 
	

//	vector<std::future<size_t>> nbShapes;
	vector<cv::Mat> framesVector;
	vector<size_t> nbrShapes{};
	vector<std::thread> threadsCapture;
	//std::mutex vector_mutex;
	vector<ats::MonitorInfo> monitorInfos;

	cv::utils::logging::setLogLevel(cv::utils::logging::LogLevel::LOG_LEVEL_SILENT);
	
	//ats::Errors errors(logAts);
	ats::Errors errors;
	errors.setAtsLog(logAts);
	OptionsData optionsData;
	OptionLigneCommande options(argc, argv, optionsData.getOptions(),errors);
	
	Resolution resolution;
	bool isWebcam = false;
	if (!options.getOptionsError()) exit(EXIT_FAILURE);

	//options.setOptionsValue("crop", "-2000, 0, 500, 500");
	//options.setOptionsValue("cap", "0");
	//options.setOptionsValue("t", "3");
 	//options.setOptionsValue("fps", "20");
	//options.setOptionsValue("rcap", "vga");
	//options.setOptionsValue("rout", "fullhd");
	//options.setOptionsValue("rec", "1");
	


	if (options.getOptionValue("t") == "0") {
		//on enregistre une image en png
		options.setOptionsValue("f", options.getOptionValue("f").substr(0, options.getOptionValue("f").size() - 4) + ".png");
	}
	
	//setting resolution capure device, input and output
	int capWidth = (int) resolution.getResolution(options.getOptionValue("rcap"))[0];
	int capHeight = (int) resolution.getResolution(options.getOptionValue("rcap"))[1];
	int inWidth = (int) resolution.getResolution(options.getOptionValue("rin"))[0];
	int inHeight = (int) resolution.getResolution(options.getOptionValue("rin"))[1];
	int outWidth = (int) resolution.getResolution(options.getOptionValue("rout"))[0];
	int outHeight = (int) resolution.getResolution(options.getOptionValue("rout"))[1];

	//setting screen
	int virtualScreenX = GetSystemMetrics(SM_XVIRTUALSCREEN);
	int virtualScreenY = GetSystemMetrics(SM_YVIRTUALSCREEN);
	int virtualScreenWidth = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	int virtualScreenHeight = GetSystemMetrics(SM_CYVIRTUALSCREEN);
	
	logAts.addLog("virtualScreenX : " + to_string(virtualScreenX));
	logAts.addLog("virtualScreenY : " + to_string(virtualScreenY));
	logAts.addLog("virtualScreenWidth : " + to_string(virtualScreenWidth));
	logAts.addLog("virtualScreenHeight : " + to_string(virtualScreenHeight));

	//handle windows
	HWND hDesktopWnd = GetDesktopWindow();
	

	//capture device
	VideoCapture cap;
	if (options.getOptionValue("cap") != "0") {
		cap = VideoCapture(getCaptureDevice(options)-1);
		if (!cap.isOpened()) { errors.displayError(1); exit(EXIT_FAILURE); }
		isWebcam = true;
	}
	else {
		RECT windowsize;    // get the height and width of the screen
		GetClientRect(hDesktopWnd, &windowsize);
		capHeight = inHeight = windowsize.bottom;
		capWidth = inWidth = windowsize.right;
		//screen info
		//EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, reinterpret_cast<LPARAM>(&monitorInfos));

	}

	logAts.addLog("capWidth : " + to_string(capWidth));
	logAts.addLog("capHeight : " + to_string(capHeight));

	//record file

	bool isRec = isTrue(options.getOptionValue("rec"));
	
	////recovery of the maximum size of the capture device set to crop by default
	/*
	cap.set(CAP_PROP_FRAME_WIDTH, 10000);
	cap.set(CAP_PROP_FRAME_HEIGHT, 10000);
	double capWidthMax = cap.get(CAP_PROP_FRAME_WIDTH);
	double capHeightMax = cap.get(CAP_PROP_FRAME_HEIGHT);
	*/
	
	//setting capture device
	cap.set(CAP_PROP_FRAME_WIDTH, capWidth);
	cap.set(CAP_PROP_FRAME_HEIGHT, capHeight);

	
	Mat img = Mat::zeros(capHeight, capWidth, CV_8UC3);


	//coefficient resolution adjustment between input and capture device
	float krx = 1.0f, kry = 1.0f;
	krx = (inWidth * 1.0f) / (capWidth * 1.0f);
	kry = (inHeight * 1.0f) / (capHeight * 1.0f);

	//if crop is not set, set to max for one screen or one webcam
	if (options.getOptionValue("crop") == "0,0,0,0") {
		options.setOptionsValue("crop", "0,0," + to_string(capWidth) + "," + to_string(capHeight));
		krx = 1.0f;
		kry = 1.0f;
	}
	logAts.addLog("krx : " + to_string(krx));
	logAts.addLog("kry : " + to_string(kry));

	//setting frame per seconds
	double fps = stod(options.getOptionValue("fps"));
	cap.set(CAP_PROP_FPS, fps);

	//setting codec
	int codecVideo;
	codecVideo = VideoWriter::fourcc(getCharCodec(options, 0), getCharCodec(options, 1), getCharCodec(options, 2), getCharCodec(options, 3));
	logAts.addLog("codec : " + options.getOptionValue("codec"));
	logAts.addLog("codecVideo : " + to_string(codecVideo));

	//setting area of capture
	Rect crop = getRectCrop(options,krx,kry);
	if ((crop.width == 0 && crop.height == 0) || crop.width < 0 || crop.height < 0) {	errors.displayError(21); exit(EXIT_FAILURE); }
	if (isWebcam) {
		//if (crop.x < 0 || crop.y < 0 || crop.x + crop.width > capWidth || crop.y + crop.height > capHeight) { errors.displayError(22); exit(EXIT_FAILURE); }
		if (crop.x < 0 ) crop.x = 0;
		if (crop.y < 0) crop.y = 0;
		if (crop.x + crop.width > capWidth) crop.width = capWidth - crop.x;
		if (crop.y + crop.height > capHeight) crop.height = capHeight - crop.y;
	}
	else {
		int maxWidth = (virtualScreenX < 0) ? virtualScreenWidth + virtualScreenX : virtualScreenWidth - virtualScreenX;
		int maxHeight = (virtualScreenY < 0) ? virtualScreenHeight + virtualScreenY : virtualScreenHeight - virtualScreenY;
		//if (crop.x < virtualScreenX || crop.y < virtualScreenY || crop.x + crop.width > maxWidth || crop.y + crop.height > virtualScreenHeight) { errors.displayError(22); exit(EXIT_FAILURE); }
		if (crop.x <= virtualScreenX) { crop.x = virtualScreenX;
	//		logAts.addLog("if (crop.x <= virtualScreenX)");
	//		logAts.addLog("crop.x : " + to_string(crop.x));
		}
		else if (crop.x >= maxWidth) { 
			//crop.x = maxWidth - crop.width; 
			crop.x = maxWidth; 
//			logAts.addLog("else if (crop.x >= maxWidth)");
//			logAts.addLog("crop.x : " + to_string(crop.x));
		}
		if (crop.y <= virtualScreenY) { crop.y = virtualScreenY; 
		//	logAts.addLog("if (crop.y <= virtualScreenY)");
		//	logAts.addLog("crop.y : " + to_string(crop.y));
		}
		else if (crop.y >= maxHeight) { 
		//crop.y = maxHeight - crop.height;
		crop.y = maxHeight;
	//	logAts.addLog("else if (crop.y >= maxHeight) ");
	//	logAts.addLog("crop.y : " + to_string(crop.y));
		}
		//if (crop.x + crop.width > maxWidth) crop.width = maxWidth - crop.x;
/*
		if (crop.x + crop.width >= virtualScreenWidth) { 
			//crop.width = virtualScreenWidth - crop.x; 
			logAts.addLog("if (crop.x + crop.width >= virtualScreenWidth) ");
			logAts.addLog("crop.width : " + to_string(crop.width));
		}
		if (crop.y + crop.height >= virtualScreenHeight) { 
			//crop.height = virtualScreenHeight - crop.y; 
			logAts.addLog("if (crop.y + crop.height >= virtualScreenHeight)  ");
			logAts.addLog("crop.height : " + to_string(crop.height));
		}
		*/
	}
	logAts.addLog("------------------------------------------------");
	logAts.addLog("crop.x : " + to_string(crop.x));
	logAts.addLog("crop.y : " + to_string(crop.y));
	logAts.addLog("crop.width : " + to_string(crop.width));
	logAts.addLog("crop.height : " + to_string(crop.height));
	
	
	
//	outWidth = crop.width;
//	outHeight = crop.height;

	//Settings file output	
	//Size frameSize(outWidth,outHeight);
	Size frameSize(crop.width, crop.height);
	string fileout = dossierTemp + options.getOptionValue("f");


	if (options.getOptionValue("test") == "getShapes") {

		int nbrFrames = 0;
		int stopCapture = getExitFps(options);
		size_t totalShapes = 0;
		size_t averageShapes = 0;
		Mat roi, roiResizeOut;

		if (options.getOptionValue("t") == "0") {

			if (!isWebcam) {
				roi = getScreenshotMultiScreen(crop.x, crop.y, crop.width, crop.height);
			}
			else {
				cap.read(img);
				roi = img(crop);
			}
			
			resize(roi, roiResizeOut, frameSize);

			//image save
			imwrite(fileout, roiResizeOut);
			totalShapes += count_shapes(roi);
			++nbrFrames;
			cap.release();
		}
		else {
			int timeWaitFps = getWaitFps(options);
			vector<size_t> avgExec {};

			// create vectors capture
			createVectors(stoi(options.getOptionValue("t")), stoi(options.getOptionValue("fps")), framesVector, nbrShapes);

			if (!isWebcam) { 
				while (nbrFrames < stopCapture) {
					threadsCapture.emplace_back(captureFrameScreen, std::ref(hDesktopWnd), std::ref(crop), std::ref(frameSize), nbrFrames, std::ref(framesVector), std::ref(nbrShapes), std::ref(isRec));
					waitKey(timeWaitFps);
					++nbrFrames;
				}
			}
			else {
				while (nbrFrames < stopCapture) {
					threadsCapture.emplace_back(captureFrameWebcam, std::ref(cap), std::ref(crop), std::ref(frameSize), nbrFrames, std::ref(framesVector), std::ref(nbrShapes), std::ref(isRec));
					waitKey(timeWaitFps);
					++nbrFrames;
				}
			}

			//construct video output if rec true
			VideoWriter outCapture;
			if(isRec) outCapture.open(fileout, codecVideo, fps, frameSize, true);
			else  fileout = ""; 
			//VideoWriter outCapture(fileout, -1, fps, frameSize);  // list des codec en fonction de l'extension du fichiers
			if (!outCapture.isOpened()) {
				logAts.addLog("Le fichier n as pas pu etre ouvert");
				logAts.addLog("fileout -> " + fileout);
				logAts.addLog("codec -> " + to_string(codecVideo));
				logAts.addLog("fps -> " + to_string(fps));
				logAts.addLog("famesize width -> " + to_string(frameSize.width));
				logAts.addLog("famesize height -> " + to_string(frameSize.height));
				exit(EXIT_FAILURE);
			}
			//wait end thread
			for(int i = 0  ;i < threadsCapture.size(); i++){

				threadsCapture[i].join();
				if (isRec) {
					outCapture.write(framesVector[i]);
					framesVector[i] = cv::Mat{};
				}
			}
			//release videoCapture cap and release VideoWriter outCapture
			cap.release();
			outCapture.release();
	
			//count nbShapres
			for (size_t m : nbrShapes) {totalShapes += m;}

		}

		//average calculation
		averageShapes = totalShapes / ((nbrFrames > 0) ? nbrFrames : 1);
		
		//output console information
		printf("shapes-count=%zu\n", totalShapes);
		printf("shapes-average=%zu\n", averageShapes);
		printf("output-file=%s\n", fileout.c_str());
	}

	else if (options.getOptionValue("test") == "getText") {
		cout << "getText" << endl;
	}

	else if (options.getOptionValue("test") == "getFaces") {
		cout << "getFaces" << endl;
	}
	else if (options.getOptionValue("test") == "getImg") {
		cout << "getImg" << endl;
	}
	return 0;
}
