#pragma once
#include <Windows.h>
#include <iostream>
#include <filesystem>
#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/core/utils/logger.hpp>
#include "optionLigneCommande.h"
#include "options.h"
#include "errors.h"
#include "resolution.h"
#include "functions.h"
#include "log.h"

using namespace cv;
using namespace ats;

namespace fs = std::filesystem;



//using std::thread;



