#include "log.h"
namespace ats {
    LogAts::LogAts() {
    
    }

    LogAts::LogAts(bool init) {
        m_sep = std::filesystem::path::preferred_separator;
        _getPath();
        _checkLogDir();
        _delLogDir(init);
	}

    bool LogAts::_getPath() {
        m_logPath = std::filesystem::temp_directory_path().string();
        if (!m_logPath.empty()) return true;
        m_logPath = std::filesystem::current_path().string();
        if (!m_logPath.empty()) return true;
        return false;
    }

    bool LogAts::_checkLogDir() {
        
        std::filesystem::path logDir = m_logPath + m_logDir;
        if (!std::filesystem::exists(logDir)) {
            return std::filesystem::create_directory(logDir);
        }
        else return true; 
    }

    bool LogAts::_delLogDir(const bool& init) {
        
        std::filesystem::path filePath = getPathLogFile();
        if (std::filesystem::exists(filePath)) {
            if(init) return std::filesystem::remove(filePath);
            else {
                addLog("--------------------------------");
            }
        }
        return true;
    }

    void  LogAts::addLog(const std::string& logMessage) {
        std::string pathLog = getPathLogFile();
        std::ofstream file(pathLog, std::ios::app);
        if (!file)  return;
        std::time_t currentTime = std::time(nullptr);
        std::tm localTime;
        localtime_s(&localTime, &currentTime);
        
        char timestamp[20];
        std::strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", &localTime); // format: YYYY-MM-DD HH:MM:SS
        file << "[" << timestamp << "] --> " << logMessage << std::endl;
//        file << logMessage << std::endl;
        file.close();
    }
}//end namespace ats