#include "options.h"
namespace ats {
	OptionsData::OptionsData() {
		string key = "";
		string value = "";
		string help = "";
		string regexValidate = "";
		int errorNumber = 0;
		bool visibility = true;

		////tl
		//key = "tl";
		//value = "0,0";
		//help = "top left catch area - x,y  \n\texample\n\t\t-tl 0,0   : sets the top point of the capture window : x =  0 pixel and y = 0 pixel ";
		//regexValidate = "^(?:[0-9]+,[0-9]+)$";
		//errorNumber = 2;
		//OptionsStruct::strOptions tl{ key,value,help,regexValidate,errorNumber };
		//m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, tl));
		//

		////br
		//key = value = help = regexValidate = "";
		//key = "br";
		//value = "100,100";
		//help = "Bottom Right catch area - x,y  \n\texample \n\t\t-br 100,100  : defines the bottom point of the capture window : x = 100 pixel and y = 100 pixel";
		//regexValidate = "^(?:[0-9]+,[0-9]+)$";
		//errorNumber = 3;
		//OptionsStruct::strOptions br{ key,value,help,regexValidate, errorNumber };
		//m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, br));

		//fps
		key = value = help = regexValidate = "";
		key = "fps";
		value = "20";
		help = "Frame per second default 25fps 1 >= x <= 25 \n\texample : \n\t\t-fps 25   : set fps at 25 framer per second";
		regexValidate = "^(?:[1-9]|1[0-9]|2[0-5])$";
		errorNumber = 4;
		visibility = true;
		OptionsStruct::strOptions fps{ key,value,help,regexValidate, errorNumber, visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, fps));

		//codec
		key = value = help = regexValidate = "";
		key = "codec";
		//value = "avc1";
		//value = "vp08";
		value = "vp09";
		//help = "Video Codecs: \n\t - mp42 : codec MPEG-4 v2.0 \n\t - mp4v : codec MPEG 4 \n\t - divx codec Motion-Jpeg OpenDML\n\t - mjpg codec MOTION Jpeg";
		help = "Video Codecs: \n\t avc1, avc3, h264";
		regexValidate = "^(?:avc1|avc3|h264|x264|vp09|av01|i420|pim1|theo|flv1|mp4v|divx)$";
		errorNumber = 5;
		visibility = false;
		OptionsStruct::strOptions codec{ key,value,help,regexValidate, errorNumber, visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, codec));

		//f  -- files out.avi
		key = value = help = regexValidate = "";
		key = "f";
		value = "ats-out.mp4";
		//value = "out.webm";
		help = "-f output file name of the capture \n\texample :\n\t\t-f video.mp4";
		//regexValidate = "^(?:[a-zA-Z][.:\\/\\w]*[.][a-zA-Z0-9]{3})$";
		regexValidate = "^(?:[a-zA-Z][.\\-:\\/\\w]*[.](mp4|avi|webm))$";
		errorNumber = 6;
		visibility = false;
		OptionsStruct::strOptions f{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, f));

		//t  -- times capture
		key = value = help = regexValidate = "";
		key = "t";
		value = "3";
		help = "Video capture duration 0 >= x <= 20 seconds ; 0 for single img in png \n\texample : \n\t\t-t 15   : set a capture of 15 seconds";
		regexValidate = "^(?:[0-9]|1[0-9]|20)$";
		//regexValidate = "^(?:[0-9]|1[0-9]|2[0-9]|30)$";
		errorNumber = 7;
		visibility = true;
		OptionsStruct::strOptions t{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, t));

		//cap  -- input device 
		key = value = help = regexValidate = "";
		key = "cap";
		value = "0";
		help = "cap set the number of the default capture device 0";
		regexValidate = "^(?:[0-9]|99|\\-1)$";
		errorNumber = 8;
		visibility = true;
		OptionsStruct::strOptions cap{ key,value,help,regexValidate,errorNumber, visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, cap));

		//rin resolution ecran capture
		key = value = help = regexValidate = "";
		key = "rin";
		value = "fullhd";
		help = "rin capture screen resolution, by default fullhd \n\tsvga : 800x600 \n\tvga : 640x480 \n\txga : 1024x768 \n\thd : 1280x720 \n\tfullhd : 1920x1080";
		regexValidate = "^(?:svga|vga|xga|hd|fullhd)$";
		errorNumber = 9;
		visibility = false;
		OptionsStruct::strOptions rin{ key,value,help,regexValidate,errorNumber, visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, rin));

		//rcap resolution device capture
		key = value = help = regexValidate = "";
		key = "rcap";
		value = "vga";
		help = "rcap capture device resolution, by default xga \n\tvga : 640x480 \n\tsvga : 800x600 \n\txga : 1024x768 \n\thd : 1280x720 \n\tfullhd : 1920x1080";
		regexValidate = "^(?:vga|svga|xga|hd|fullhd)$";
		errorNumber = 10;
		visibility = false;
		OptionsStruct::strOptions rcap{ key,value,help,regexValidate,errorNumber, visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, rcap));

		//rout resolution files output
		key = value = help = regexValidate = "";
		key = "rout";
		value = "vga";
		help = "rout file output resolution, by default qvga \n\tcga : 320x200 \n\tqvga : 320x240 \n\tvga : 640x480 \n\tsvga : 800x600 \n\txga : 1024x768 \n\thd : 1280x720 \n\tfullhd : 1920x1080";
		regexValidate = "^(?:cga|qvga|vga|svga|xga|hd|fullhd)$";
		errorNumber = 11;
		visibility = false;
		OptionsStruct::strOptions rout{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, rout));

		//crop area of capture
		key = value = help = regexValidate = "";
		key = "crop";
		value = "0,0,0,0";
		help = "crop defines the area to be captured\n\t value : x,y,width,height example : -crop 0,0,100,100";
		regexValidate = "^(?:[\\-]?[0-9]+,[\\-]?[0-9]+,[0-9]+,[0-9]+)$";
		errorNumber = 12;
		visibility = true;
		OptionsStruct::strOptions crop{ key,value,help,regexValidate,errorNumber, visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, crop));

		//v displays the capture window
		key = value = help = regexValidate = "";
		key = "verbose";
		value = "0";
		help = "v set to display the capture windows default at 0 no display";
		regexValidate = "^(?:[0-1]{1})$";
		errorNumber = 13;
		visibility = false;
		OptionsStruct::strOptions verbose{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, verbose));

		//outweb output to website in mp4 format
		key = value = help = regexValidate = "";
		key = "outweb";
		value = "0";
		help = "outweb set to display the capture in web mp4 format mp4 default at 1 for web - 0 for other use";
		regexValidate = "^(?:[0-1]{1})$";
		errorNumber = 14;
		visibility = false;
		OptionsStruct::strOptions outweb{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, outweb));

		//test test to run
		key = value = help = regexValidate = "";
		key = "test";
		value = "getShapes";
		help = "test defines the test to run. By default the application calculates the number of images to detect in a capture window.";
		regexValidate = "^(?:getShapes|getText|getFaces|getImg)$";
		errorNumber = 15;
		visibility = false;
		OptionsStruct::strOptions test{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, test));

		//imgSrc
		key = value = help = regexValidate = "";
		key = "imgSrc";
		value = "0";
		help = "imgSrc defines the relative path or url of an image in jpg, bmp or png format.";
		regexValidate = "(^(http[s]?)://.*([.](png|jpg|bmp))$)|(^[cdefCDEF]{1}:([.\\a-zA-Z0-9])*([.](png|jpg|bmp))$)|^0$";
		//regexValidate = "(^(http[s]?):\/\/.*([.](png|jpg|bmp))$)|(^[cdefCDEF]{1}:(\\\\w*)*([.](png|jpg|bmp))$)";
		errorNumber = 16;
		visibility = false;
		OptionsStruct::strOptions imgSrc{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, imgSrc));

		//TypeimgSrc
		key = value = help = regexValidate = "";
		key = "typeImgSrc";
		value = "url";
		help = "typeImgSrc defines the type of the image src either url or path.";
		regexValidate = "^(?:url|path)$";
		errorNumber = 17;
		visibility = false;
		OptionsStruct::strOptions typeImgSrc{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, typeImgSrc));

		//rec
		key = value = help = regexValidate = "";
		key = "rec";
		value = "true";
		help = "rec save video.";
		regexValidate = "^(?:[tT][rR][uU][eE]|[fF][aA][lL][sS][eE]|[01])$";
		errorNumber = 18;
		visibility = false;
		OptionsStruct::strOptions rec{ key,value,help,regexValidate,errorNumber,visibility };
		m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, rec));
	}
}