#pragma once
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "optionLigneCommande.h"
#include <opencv2/opencv.hpp>
#include <Windows.h>
#include <regex>

namespace ats {

	/*!
	* @brief struct MonitorInfo
	* Information about a monitor
	*/
	struct MonitorInfo {
		std::string name;
		int width;
		int height;
		int left;
		int top;
		int orientation;
		bool isPrimary;
		bool isExtended;
		int scale;
	};

	/*!
	* \brief ats_openCV functions
	*
	* Functions for ats_openCV
	*/

	/*!
	* \brief getCaptureDevice
	*
	* Get capture device in options struct
	*
	* \param options : struct of OptionLigneCommande
	* \return int : value betwenn 0 and 9
	*/
	const int getCaptureDevice(OptionLigneCommande& options);

	/*!
	* \brief  getCharCodec
	* Get char codec in options struct
	*
	* \param options : struct of OptionLigneCommande
	* \param p : position of char in string
	* \return char in Upper for codec fourCC.
	*/
	const char getCharCodec(OptionLigneCommande& options, int p);

	/*!
	* \brief  getFps
	* Get Frames per second in options struct
	*
	* \param options : struct of OptionLigneCommande
	* \return double value netween 1 and 30
	*/
	const double getFps(OptionLigneCommande& options);

	/*!
	* \brief  getSizeCapture
	* Get size of capture in options struct
	*
	* \param options : struct of OptionLigneCommande
	* \return Size2i
	*/
	const cv::Size2i getSizeCapture(OptionLigneCommande& options);

	/*!
	* \brief  getWaitFps
	* Get wait time in options struct, time in ms
	* \param options : struct of OptionLigneCommande
	* \return int
	*/
	const int getWaitFps(OptionLigneCommande& options);

	/*!
	* \brief  getExitFps
	* Get exit time in options struct, time in ms
	* \param options : struct of OptionLigneCommande
	* \return int number of frames for exit loop
	*/
	const int getExitFps(OptionLigneCommande& options);

	/*!
	* \brief  getRectCrop
	* Get rect crop in options struct
	* \param options : struct of OptionLigneCommande
	* \param krx : ratio x
	* \param kry : ratio y
	* \return cv::Rect in the format of the capture area
	*/
	const cv::Rect getRectCrop(OptionLigneCommande& options, float& krx, float& kry);

	/*!
	* \brief avi2mp4
	* Avi to mp4 conversion - running ffmpeg thread without output
	* output redirect to null
	* \param filename : name of avi file
	* \param directory : directory of avi file
	*/
	void avi2mp4(std::string inputFile, std::string directory);

	/*!
	*\brief count_shapes
	* Count shapes in frame
	* \param frame : frame of video in grayScale
	* \return int number of shapes
	*/
	size_t count_shapes(cv::Mat frame);


	/*!	
	* @brief hwnd2Mat
	* local capture screen
	* @param hwnd : handle of window
	* @return cv::Mat of screen
	*/
	cv::Mat hwnd2Mat(HWND hwnd);

	/*!
	* @brief createVectors
	* create vector of frames and vector of shapes size of time * fps
	* @param t : time in second
	* @param fps : frames per second
	* @param framesVector : vector of frames
	* @param nbShapesVector : vector of shapes
	
	* 
	*/
	void createVectors(const int& t, const int& fps, std::vector<cv::Mat>& framesVector, std::vector<size_t>& nbShapesVector);
	
	/*!
	* @brief captureFrameWebcam 
	* capture webcam frame in video for thread
	* @param cap : video capture
	* @param crop : rect crop
	* @param frameSize : size of frame
	* @param frameNumber : number of frame
	* @param framesVector : vector of frames
	* @param nbShapes : vector of shapes
	* @param isRec : is recording
	*/
	void captureFrameWebcam(cv::VideoCapture& cap, cv::Rect& crop, cv::Size& frameSize, int frameNumber, std::vector<cv::Mat>& framesVector, std::vector<size_t>& nbShapes, const bool& isRec);

	/*!
	* @brief captureFrameScreen
	* capture screen frame in video for thread
	* @param hwnd : handle of window
	* @param crop : rect crop
	* @param frameSize : size of frame
	* @param frameNumber : number of frame
	* @param framesVector : vector of frames
	* @param nbShapes : vector of shapes
	* @param isRec : is recording
	*/
	void captureFrameScreen(HWND& hwnd, cv::Rect& crop, cv::Size& frameSize, int frameNumber, std::vector<cv::Mat>& framesVector, std::vector<size_t>& shapesVector, const bool& isRec);

	/*!
	* @brief isTrue
	* check if string value is true
	* @param str : string to check
	* @return bool
	*/
	bool isTrue(string str);
	
	/*!
	* @brief MonitorEnumProc
	* methode CallBack for EnumDisplayMonitors
	* @param hMonitor : handle of monitor
	* @param hdcMonitor : handle of device context
	* @param lprcMonitor : pointer to a RECT structure that specifies the display monitor rectangle, expressed in virtual-screen coordinates
	* @param dwData : pointer to application-defined data
	* @return BOOL : If the function succeeds, the return value is nonzero. If the function fails, the return value is zero. To get extended error information, call GetLastError.
	*/
	BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData);
	

	cv::Mat getScreenshotMultiScreen(const int& x, const int& y, const int& width, const int& height);

	std::string getOpenCvArg(int argc, char* argv[]);
}
#endif // !FUNCTIONS_H

