#include "errors.h"

namespace ats {
	
	Errors::Errors(){
		ErrorsData::ErrorInfo error;
		error.errorNumber = 1;
		error.errorMessage = "The input device is not active or detected.\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		/////////////// Errors Options //////////////////
		//options tl
		error.errorNumber = 2;
		error.errorMessage = "Wrong input format in options tl format x,y (0,0)\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options br
		error.errorNumber = 3;
		error.errorMessage = "Wrong input format in options br format x,y (0,0)\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options fps
		error.errorNumber = 4;
		error.errorMessage = "Wrong input format in options fps\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options codec
		error.errorNumber = 5;
		error.errorMessage = "Wrong input format in options codec, codec value xvid,mp42,mp4v,divx,mjpg\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options output files
		error.errorNumber = 6;
		error.errorMessage = "Wrong input format in options f\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options times
		error.errorNumber = 7;
		error.errorMessage = "Wrong input format in options t - times capture between 0 and 20 seconds\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options capture
		error.errorNumber = 8;
		error.errorMessage = "Wrong input format in options cap - capture between 0 and 9\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options rin resolution display 
		error.errorNumber = 9;
		error.errorMessage = "Wrong input format in options rin - vga,xga,svga,hd,fullhd\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options rcap resolution display capture
		error.errorNumber = 10;
		error.errorMessage = "Wrong input format in options rcap - vga,xga,svga,hd,fullhd\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options rout resolution display capture
		error.errorNumber = 11;
		error.errorMessage = "Wrong input format in options rout - vga,xga,svga,hd,fullhd\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options crop area capture
		error.errorNumber = 12;
		error.errorMessage = "Wrong input format in options crop - the input format is x,y,height, width  example -crop 0,0,100,100\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options crop area capture
		error.errorNumber = 13;
		error.errorMessage = "Wrong input format in options v - the input format is 0 or 1\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options crop area capture
		error.errorNumber = 14;
		error.errorMessage = "Wrong input format in options outweb - the input format is 0 or 1\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options test 
		error.errorNumber = 15;
		error.errorMessage = "Wrong input format in options test - the input format is getShapes | getText | getFace | getImg\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options imgSrc
		error.errorNumber = 16;
		error.errorMessage = "Wrong input format in options imgSrc - the input format is url or path with img filename png, bmp or jpg format\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		//options typeImgSrc
		error.errorNumber = 17;
		error.errorMessage = "Wrong input format in options typeImgSrc - the input format url or path \n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);


		//options rec
		error.errorNumber = 18;
		error.errorMessage = "Wrong input format in options rec - the input format true or false\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		error.errorNumber = 20;
		error.errorMessage = "Error opening output video file.\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		error.errorNumber = 21;
		error.errorMessage = "Error crop height and width = 0.\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

		error.errorNumber = 22;
		error.errorMessage = "The capture area is outside the capture window.\n";
		error.gravity = CRITICAL;
		m_errors.push_back(error);

	};

	const ErrorsData::ErrorInfo Errors::getError(int errorNumber) {
		for (auto a : m_errors) {
			if (a.errorNumber == errorNumber) return a;
		}
		return ErrorsData::ErrorInfo();
	};


	void Errors::displayError(int errorNumber) {
		m_log.addLog("error-code=" + errorNumber);
		m_log.addLog("error-message=" + getError(errorNumber).errorMessage);
		cout << "error-code= " << errorNumber << "\n";
		cout << "error-message= " << getError(errorNumber).errorMessage << endl;
	}
}