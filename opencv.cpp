#include <opencv2/opencv.hpp>
//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc.hpp>
//#include <opencv2/core/utils/logger.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
	//cv::utils::logging::setLogLevel(cv::utils::logging::LogLevel::LOG_LEVEL_SILENT);

	int duration = 3;
	double frameRates = 16;
	Rect crop = Rect(0, 0, 200, 200);

	std::string screenFilePath;

	char* buf = nullptr;
	size_t sz = 0;
	if (_dupenv_s(&buf, &sz, "TEMP") == 0 && buf != nullptr)
	{
		std::string path(buf);
		free(buf);

		path.append("\\ats-screen.mp4");
		screenFilePath = path.c_str();
	}
		
	VideoCapture capture = VideoCapture(0);

	capture.set(3, 10000.0);
	capture.set(4, 10000.0);

	double w = capture.get(3);
	double h = capture.get(4);
	Size size = Size((int)w, (int)h);

	int fourcc = VideoWriter::fourcc('m', 'p', '4', 'v');

	VideoWriter writer = VideoWriter(screenFilePath.c_str(), fourcc, frameRates, size, true);

	Mat frame = Mat::zeros(w, h, CV_8UC3);
	
	int n = duration * frameRates;
	printf("frames-count=%i\n", n);

	while (n > 0) {
		capture.read(frame);
		writer.write(frame);
		n--;
	}

	writer.release();

	printf("screen-file=%s\n", screenFilePath.c_str());
	printf("device-size=%ix%i\n", size.width, size.height);
	printf("crop-rect=%ix%ix%ix%i\n", crop.x, crop.y, crop.width, crop.height);
	printf("duration=%i\n", duration);
	printf("shapes-count=%i\n", 0);

	return 0;
}