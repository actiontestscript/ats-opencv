#include "functions.h"
namespace ats {
	const int getCaptureDevice(OptionLigneCommande& options) { return stoi(options.getOptionValue("cap")); }

	const char getCharCodec(OptionLigneCommande& options, int p) {
		return toupper(options.getOptionValue("codec")[p]);
	}

	const double getFps(OptionLigneCommande& options) { return stod(options.getOptionValue("fps")); }

	const cv::Size2i getSizeCapture(OptionLigneCommande& options) {
		int tl_x = 0, br_x = 0, tl_y = 0, br_y = 0;
		size_t p;
		string tl = options.getOptionValue("tl");
		string br = options.getOptionValue("br");
		p = tl.find(',');
		if (p != tl.npos) {
			tl_x = stoi(tl.substr(0, p));
			tl_y = stoi(tl.substr(p + 1));
		}
		p = br.find(',');
		if (p != br.npos) {
			br_x = stoi(br.substr(0, p));
			br_y = stoi(br.substr(p + 1));
		}
		cv::Size2i r;
		r.width = abs(tl_x - br_x);
		r.height = abs(tl_y - br_y);
		return r;
	}

	const int getWaitFps(OptionLigneCommande& options) { return (int)(1000 / stoi(options.getOptionValue("fps"))); }
	const int getExitFps(OptionLigneCommande& options) { return (int)((int)stoi(options.getOptionValue("fps")) * (int)stoi(options.getOptionValue("t"))); }

	const cv::Rect getRectCrop(OptionLigneCommande& options, float& krx, float& kry) {
		string s = options.getOptionValue("crop");
		size_t p;
		int x = 0, y = 0, width = 0, height = 0;
		p = s.find(',');
		x = (int)(stoi(s.substr(0, p)) / krx);
		s = s.substr(p + 1);
		p = s.find(',');
		y = (int)(stoi(s.substr(0, p)) / kry);
		s = s.substr(p + 1);
		p = s.find(',');
		width = (int)(stoi(s.substr(0, p)) / krx);
		height = (int)(stoi(s.substr(p + 1)) / kry);
		//check if pair
		if(width % 2 != 0) --width;
		if(height % 2 != 0) --height;
		cv::Rect r(x, y, width, height);
		return r;
	}

	void avi2mp4(std::string inputFile, std::string directory) {
		std::string fullInputFile = directory + inputFile;
		std::string fullOutputFile = directory + inputFile.substr(0, inputFile.size() - 4) + ".mp4";
		std::string command = "ffmpeg -i " + fullInputFile + " -y -c:v libx264 -crf 23 -c:a aac -b:a 192k -strict -2 " + fullOutputFile + " > NUL 2>&1";
		system(command.c_str());
	}


	size_t count_shapes(cv::Mat frame) {
		cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
		threshold(frame, frame, 150, 255, cv::THRESH_OTSU);
		vector<vector <cv::Point>> formesContours;
		vector<cv::Vec4i> hierarchy;
		findContours(frame, formesContours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);

		return formesContours.size();
	}

	void createVectors(const int& t, const int& fps, std::vector<cv::Mat>& framesVector, std::vector<size_t>& nbShapesVector) {
		int nbElements = t * fps;
		nbElements;
		for (int i = 0; i < nbElements; i++) {
			framesVector.push_back(cv::Mat{});
			nbShapesVector.push_back(0);
		}
	}

	void captureFrameWebcam(cv::VideoCapture& cap, cv::Rect& crop, cv::Size& frameSize, int frameNumber,  std::vector<cv::Mat> &framesVector, std::vector<size_t> &shapesVector, const bool& isRec) {
		cv::Mat imgSrc;
		cap.read(imgSrc);
		cv::Mat roi = imgSrc(crop);
		shapesVector[frameNumber] = count_shapes(roi);
		if (isRec) {
			resize(roi, framesVector[frameNumber], frameSize);

		}
	}

	void captureFrameScreen(HWND &hwnd, cv::Rect& crop, cv::Size& frameSize, int frameNumber, std::vector<cv::Mat>& framesVector, std::vector<size_t> &shapesVector, const bool& isRec) {
		cv::Mat roi = getScreenshotMultiScreen(crop.x, crop.y, crop.width, crop.height);
		shapesVector[frameNumber] = count_shapes(roi);
		if (isRec) {
			resize(roi, framesVector[frameNumber], frameSize);
		}
	}

	bool isTrue(string str) {
		std::regex pattern("^(?:[tT][rR][uU][eE]|[1])$");
		return std::regex_match(str, pattern);
	}

	BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
	{
		MONITORINFOEX monitorInfo;
		monitorInfo.cbSize = sizeof(monitorInfo);
		GetMonitorInfo(hMonitor, &monitorInfo);
		char buffer[32];
		WideCharToMultiByte(CP_ACP, 0, monitorInfo.szDevice, -1, buffer, 32, NULL, NULL);
		ats::MonitorInfo info{};
		info.name = std::string(buffer);

			
		info.width = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
		info.height = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;
		info.left = monitorInfo.rcMonitor.left;
		info.top = monitorInfo.rcMonitor.top;
		info.orientation = monitorInfo.dwFlags& MONITORINFOF_PRIMARY ? GetSystemMetrics(SM_CXSCREEN) > monitorInfo.rcMonitor.right ? 0 : 1 : monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top > monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left ? 0 : 1;
		info.isPrimary = monitorInfo.dwFlags & MONITORINFOF_PRIMARY;
		int numMonitors = GetSystemMetrics(SM_CMONITORS);
		info.isExtended = GetSystemMetrics(SM_SAMEDISPLAYFORMAT) == 0 || numMonitors > 1;

		//get scale factor for screen
		HDC monitorDC = CreateDC(monitorInfo.szDevice, NULL, NULL, NULL);
		int dpiX = GetDeviceCaps(monitorDC, LOGPIXELSX);
		int dpiY = GetDeviceCaps(monitorDC, LOGPIXELSY);
		DeleteDC(monitorDC);

		info.scale = static_cast<int>(dpiX / 96.0 * 100);


		std::vector<ats::MonitorInfo>* monitors = reinterpret_cast<std::vector<ats::MonitorInfo>*>(dwData);
		monitors->push_back(info);
		return TRUE;
	}

	cv::Mat hwnd2Mat(HWND hwnd) {
		HDC hwindowDC, hwindowCompatibleDC;

		int height, width, srcheight, srcwidth;
		HBITMAP hbwindow;
		cv::Mat src;
		BITMAPINFOHEADER  bi;

		hwindowDC = GetDC(hwnd);
		hwindowCompatibleDC = CreateCompatibleDC(hwindowDC);
		SetStretchBltMode(hwindowCompatibleDC, COLORONCOLOR);

		RECT windowsize;    // get the height and width of the screen
		GetClientRect(hwnd, &windowsize);

		float valueChangeSettingWindows = 1.0f;
		srcheight = (int)((float)windowsize.bottom * valueChangeSettingWindows);
		srcwidth = (int)((float)windowsize.right * valueChangeSettingWindows);
		//srcheight = windowsize.bottom;
		//srcwidth = windowsize.right;
		height = windowsize.bottom;  //change this to whatever size you want to resize to
		width = windowsize.right;

		src.create(height, width, CV_8UC4);

		// create a bitmap
		hbwindow = CreateCompatibleBitmap(hwindowDC, width, height);
		bi.biSize = sizeof(BITMAPINFOHEADER);
		bi.biWidth = width;
		bi.biHeight = -height;  //this is the line that makes it draw upside down or not
		bi.biPlanes = 1;
		bi.biBitCount = 32;
		bi.biCompression = BI_RGB;
		bi.biSizeImage = 0;
		bi.biXPelsPerMeter = 0;
		bi.biYPelsPerMeter = 0;
		bi.biClrUsed = 0;
		bi.biClrImportant = 0;

		// use the previously created device context with the bitmap
		SelectObject(hwindowCompatibleDC, hbwindow);
		// copy from the window device context to the bitmap device context
		StretchBlt(hwindowCompatibleDC, 0, 0, width, height, hwindowDC, 0, 0, srcwidth, srcheight, SRCCOPY); //change SRCCOPY to NOTSRCCOPY for wacky colors !
		GetDIBits(hwindowCompatibleDC, hbwindow, 0, height, src.data, (BITMAPINFO*)&bi, DIB_RGB_COLORS);  //copy from hwindowCompatibleDC to hbwindow

		// avoid memory leak
		DeleteObject(hbwindow);
		DeleteDC(hwindowCompatibleDC);
		ReleaseDC(hwnd, hwindowDC);

		return src;
	}


	cv::Mat getScreenshotMultiScreen(const int& x, const int& y,const int& width, const int &height) {

		// Cr�er une capture d'�cran de la zone de l'�cran
		HDC screenDC = GetDC(NULL);
		HDC captureDC = CreateCompatibleDC(screenDC);
		HBITMAP captureBitmap = CreateCompatibleBitmap(screenDC, width, height);
		SelectObject(captureDC, captureBitmap);
		BitBlt(captureDC, 0, 0, width, height, screenDC, x, y, SRCCOPY);

		// Convertir la capture d'�cran en une image OpenCV
		cv::Mat screenshot = cv::Mat(height, width, CV_8UC4);
		GetBitmapBits(captureBitmap, width * height * 4, screenshot.data);
		
		// Lib�rer les ressources
		ReleaseDC(NULL, screenDC);
		DeleteDC(captureDC);
		DeleteObject(captureBitmap);
		return screenshot;
	}

	std::string getOpenCvArg(int argc, char* argv[]) {
		std::string openCvArg{ "" };
		for (int i = 0; i < argc; ++i) {
			openCvArg += argv[i];
			if (i < argc - 1) openCvArg += " ";
		}
		return openCvArg;
	}

}
